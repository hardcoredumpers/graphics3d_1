#version 330
 
in vec3 a_vertex;
in vec2 a_uv;

out vec2 v_uv;

uniform mat4 Model;
uniform mat4 Projection;
uniform mat4 View;

void main(void)
{
	v_uv = a_uv;

	// position of the vertex
	gl_Position =  Projection * View * Model * vec4(a_vertex , 1.0);
}
